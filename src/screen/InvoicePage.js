import React from "react";
import Header from "../component/Header";
import {
  Row,
  Col,
  Card,

} from "reactstrap";

const InvoicePage = () => {
  return (
    <div style={{ overflow: "hidden" }}>
      <Header />
      <div style={styles.listcontainer}>
        <Row form style={{ marginBottom: 48 }}>
          <Col md="12">
            <Card style={{ padding: 48 }}>
              <Row style={{ flex: 1, justifyContent: "space-between" }}>
                <Col md="4">
                  <h1>Logo</h1>
                  <p>Company name</p>
                  <p>Company address</p>
                  <p>TAX ID</p>
                  <h3>BILL TO</h3>
                  <p>Company name</p>
                  <p>Company address</p>
                  <p>TAX ID</p>
                </Col>
                <Col md="4">
                  <h1>Invoice</h1>
                  <p>No</p>
                  <p>Issue Date</p>
                  <p>Due Date</p>
                </Col>
              </Row>
              <Row style={{ flex: 1, justifyContent: "space-between" }}>
                <h1>Table</h1>
              </Row>
              <Row style={{ flex: 1, justifyContent: "space-between" }}>
                <Col md="6">
                  <h1>Note</h1>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                  </p>
                </Col>
                <Col md="4">
                  <h1>Cal</h1>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default InvoicePage;

const styles = {
  listcontainer: {
    padding: "32px 80px",
  },
};
