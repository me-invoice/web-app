import React from "react";
import Header from "../component/Header";
import { Row, Col, Card, CardBody, CardImg } from "reactstrap";

import FlatList from "flatlist-react";

const data = [
  {
    id: "ID-2-27",
    name: "Invoice 1",
    customer: "A company",
    date: "1-2-3",
    due: "1-2-3",
  },
  {
    id: "ID-2-28",
    name: "Invoice 2",
    customer: "A company",
    date: "1-2-3",
    due: "1-2-3",
  },
  {
    id: "ID-2-29",
    name: "Invoice 3",
    customer: "B company",
    date: "1-2-3",
    due: "1-2-3",
  },
];
const HomePage = () => {
  const renderList = (item, idx) => {
    return (
      <div key={idx} fluid style={styles.card}>
        <Row>
          <Card style={{ width: "100%" }}>
            <Row>
              <Col xs="auto">
                <div style={styles.number}>
                  <p style={{ fontSize: 18, color: "#aaa", margin: 0 }}>No.</p>
                  <p style={{ fontSize: 18, color: "#424242" }}>{item.id}</p>
                </div>
              </Col>
              <Col>
                <CardBody style={{ padding: "0px 35px" }}>
                  <Row style={{ justifyContent: "space-between" }}>
                    <Col md="4" style={{}}>
                      <p>{item.name}</p>
                      <p>{item.customer}</p>
                    </Col>
                    <Col md="auto" style={{ padding: "28px 0px" }}>
                      <div style={styles.dateBlock}>
                        <Row>
                          <p>Issue Date: </p>
                          <p>{item.date}</p>
                        </Row>
                      </div>
                      <div style={styles.dueBlock}>
                        <Row>
                          <p>Due Date: </p>
                          <p>{item.due}</p>
                        </Row>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
              </Col>
            </Row>
          </Card>
        </Row>
      </div>
    );
  };

  return (
    <div style={{ overflow: "hidden" }}>
      <Header />
      <div style={styles.listcontainer}>
        {/* List */}
        <FlatList list={data} renderItem={renderList} />
      </div>
    </div>
  );
};

export default HomePage;

const styles = {
  listcontainer: {
    padding: "32px 80px",
  },
  card: {
    // backgroundColor: "#aaa",
    paddingTop: "12px",
    // borderBottom: "1px solid #333",
    marginBottom: "18px",
  },
  number: {
    borderRadius: "8px 0 0 8px",
    backgroundColor: "#eee",
    height: "130px",
    width: "138px",
    padding: "33px",
  },
  dateBlock: {
    backgroundColor: "#E7EFE9",
    borderRadius: "40px",
    height: "35px",
    width: "236px",
    padding: "4px 32px",
  },
  dueBlock: {
    backgroundColor: "#A7CCB0",
    borderRadius: "40px",
    height: "35px",
    width: "236px",
    padding: "4px 32px",
    marginTop: "4px",
  },
  date: {
    fontWeight: "bold",
  },
  due: {
    fontWeight: "bold",
  },
};
