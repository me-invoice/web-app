import React from "react";
import Header from "../component/Header";
import {
  Row,
  Col,
  Card,
  Form,
  FormGroup,
  Label,
  Input,
  CustomInput,
  Button,
  Table,
} from "reactstrap";
import { useHistory } from "react-router-dom";
import MaterialTable from "material-table";

const CreatePage = () => {
  const history = useHistory();

  return (
    <div style={{ overflow: "hidden",overflowY:'hidden' }}>
      <Header />
      <div style={styles.listcontainer}>
        <Form>
          <Row form style={{ marginBottom: 48 }}>
            <Col md="6">
              <p>Company Information</p>
              <Card style={{ padding: 48, height: 660 }}>
                <FormGroup>
                  <Label for="CompanyName">Name</Label>
                  <Input type="text" name="CompanyName" id="CompanyName" />
                </FormGroup>
                <FormGroup>
                  <Label for="CompanyTaxId">Tax Id</Label>
                  <Input type="text" name="CompanyTaxId" id="CompanyTaxId" />
                </FormGroup>
                <FormGroup>
                  <Label for="CompanyEmail">E-mail Address</Label>
                  <Input type="email" name="Companyemail" id="CompanyEmail" />
                </FormGroup>
                <FormGroup>
                  <Label for="CompanyPhone">Phone Number</Label>
                  <Input type="text" name="Companyphone" id="CompanyPhone" />
                </FormGroup>
                <FormGroup>
                  <Label for="CompanyLogo">Logo</Label>
                  <CustomInput
                    type="file"
                    id="CompanyLogo"
                    name="CompanyLogo"
                    label=" "
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="CompanyAddress">Address</Label>
                  <Input
                    type="textarea"
                    name="CompanyAddress"
                    id="CompanyAddress"
                  />
                </FormGroup>
              </Card>
            </Col>
            <Col md="6">
              <p>Your Customer</p>
              <Card style={{ padding: 48, height: 660 }}>
                <FormGroup>
                  <Label for="CustomerName">Name</Label>
                  <Input type="text" name="CustomerName" id="CustomerName" />
                </FormGroup>
                <FormGroup>
                  <Label for="CustomerTaxId">Tax Id</Label>
                  <Input type="text" name="CustomerTaxId" id="CustomerTaxId" />
                </FormGroup>
                <FormGroup>
                  <Label for="CustomerEmail">E-mail Address</Label>
                  <Input type="email" name="Customeremail" id="CustomerEmail" />
                </FormGroup>
                <FormGroup>
                  <Label for="CustomerPhone">Phone Number</Label>
                  <Input type="text" name="Customerphone" id="CustomerPhone" />
                </FormGroup>
                <FormGroup>
                  <Label for="CustomerAddress">Address</Label>
                  <Input
                    type="textarea"
                    name="CustomerAddress"
                    id="CustomerAddress"
                    style={{ height: 168 }}
                  />
                </FormGroup>
              </Card>
            </Col>
          </Row>
          <Row style={{ marginBottom: 48 }}>
            <Col md="12">
              <p>Invoice Information</p>
              <Card style={{ padding: 48 }}>
                <Row>
                  <Col
                    style={{
                      paddingRight: 32,
                      borderRight: "1px solid #eee",
                    }}
                  >
                    <FormGroup>
                      <Label for="InvoiceNo">No.</Label>
                      <Input type="text" name="InvoiceNo" id="InvoiceNo" />
                    </FormGroup>
                    <FormGroup>
                      <Label for="IssueDate">Issue Date</Label>
                      <Input type="text" name="IssueDate" id="IssueDate" />
                    </FormGroup>
                    <FormGroup>
                      <Label for="DueDate">Due Date</Label>
                      <Input type="text" name="DueDate" id="DueDate" />
                    </FormGroup>
                  </Col>
                  <Col style={{ paddingLeft: 32 }}>
                    <FormGroup>
                      <Label for="InvoiceNote">Note</Label>
                      <Input
                        type="textarea"
                        name="InvoiceNote"
                        id="InvoiceNote"
                        style={{ height: 210 }}
                      />
                    </FormGroup>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row style={{ marginBottom: 48 }}>
            <Col md="12">
              <p>Items</p>
              <Card style={{ padding: 64 }}>
                <MaterialTable
                  title=""
                  options={{
                    search: false,
                  }}
                  columns={[
                    { title: "Name", field: "name" },
                    { title: "Surname", field: "surname" },
                    {
                      title: "Birth Year",
                      field: "birthYear",
                      type: "numeric",
                    },
                    {
                      title: "Birth Place",
                      field: "birthCity",
                      lookup: { 34: "İstanbul", 63: "Şanlıurfa" },
                    },
                  ]}
                  data={[
                    {
                      name: "Mehmet",
                      surname: "Baran",
                      birthYear: 1987,
                      birthCity: 63,
                    },
                    {
                      name: "Zerya Betül",
                      surname: "Baran",
                      birthYear: 2017,
                      birthCity: 34,
                    },
                  ]}
                  actions={[
                    {
                      icon: "add",
                      tooltip: "Add User",
                      isFreeAction: true,
                      onClick: (event) => alert("You want to add a new row"),
                    },
                  ]}
                />
              </Card>
            </Col>
          </Row>
        </Form>
        <Row style={{ justifyContent: "center" }}>
          <Button
            style={{ height: 40, width: 683, backgroundColor: "#74957C" }}
            onClick={() => {
              history.push("/invoice");
            }}
          >
            <p>Submit</p>
          </Button>
        </Row>
      </div>
    </div>
  );
};

export default CreatePage;

const styles = {
  listcontainer: {
    padding: "32px 80px",
  },
};
