import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HomePage from "../screen/HomePage";
import CreatePage from "../screen/CreatePage";
import InvoicePage from "../screen/InvoicePage";

export default function MainRouter() {
  return (
    <Router>
      {/* A <Switch> looks through its children <Route>s and
              renders the first one that matches the current URL. */}
      <Switch>
        <Route path="/create" component={CreatePage} />
        <Route path="/invoice" component={InvoicePage} />
        <Route path="/" component={HomePage} />
      </Switch>
    </Router>
  );
}
