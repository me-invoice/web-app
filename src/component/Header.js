import React, { useState } from "react";
import { Jumbotron, Container, Row, Button } from "reactstrap";
import "../assets/font.css";
import { useHistory } from "react-router-dom";

const Header = () => {
  const history = useHistory();
  const [currentpage, setcurrentpage] = useState("list");

  console.log("window.location.href", window.location.href);
  return (
    <div style={styles.container}>
      <Jumbotron style={styles.jumbotron}>
        <Row
          style={{
            justifyContent: "space-between",
            alignItems: "center",
            padding: "16px 16%",
          }}
        >
          <Row>
            <h1 style={styles.me}>Me</h1>
            <h1 style={styles.invoice}>Invoice</h1>
          </Row>
          <Row>
            <div
              style={
                window.location.pathname === "/" &&
                window.location.pathname !== "/create" &&
                window.location.pathname !== "/invoice"
                  ? styles.active
                  : styles.inactive
              }
              onClick={() => {
                history.push("/");
              }}
            >
              <h2>LIST</h2>
            </div>
            {/* <div style={{ paddingRight: 64 }} /> */}
            <div
              style={
                window.location.pathname !== "/" &&
                window.location.pathname === "/create" &&
                window.location.pathname !== "/invoice"
                  ? styles.active
                  : styles.inactive
              }
              onClick={() => {
                history.push("/create");
              }}
            >
              <h2>CREATE</h2>
            </div>
          </Row>
        </Row>
      </Jumbotron>
    </div>
  );
};

export default Header;

const styles = {
  container: {},
  jumbotron: {
    backgroundColor: "#fff",
    height: "120px",
    padding: 0,
  },
  row: {
    padding: 24,
  },
  me: {
    // fontFamily: "",
    color: "#707070",
  },
  invoice: {
    // fontFamily: "Doris",
    color: "#9DC6A7",
  },
  active: {
    backgroundColor: "#A7CCB0",
    width: "165px",
    height: "56px",
    textAlign: "center",
    padding: "5px 0px",
    borderRadius: "8px",
    color: "#fff",
    cursor: "pointer",
  },
  inactive: {
    backgroundColor: "#fff",
    width: "165px",
    height: "56px",
    textAlign: "center",
    padding: "5px 0px",
    borderRadius: "8px",
    cursor: "pointer",
  },
};
